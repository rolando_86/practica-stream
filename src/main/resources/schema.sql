CREATE TABLE IF NOT EXISTS `webfluxdb`.`product` (
                                       `id` INT NOT NULL AUTO_INCREMENT,
                                       `name` VARCHAR(10) NULL,
                                       `price` FLOAT NULL,
                                       PRIMARY KEY (`id`),
                                       UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE);