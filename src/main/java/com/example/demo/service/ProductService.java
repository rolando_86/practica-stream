package com.example.demo.service;

import com.example.demo.dto.ProductDto;
import com.example.demo.entity.Product;
import com.example.demo.exception.CustomException;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProductService {

    private final static String NF_MESSAGE = "product not found";
    private final static String NAME_MESSAGE = "product name already in use";

    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository){
        this.productRepository = productRepository;
    }

    public Flux<Product> getAll(){
        return productRepository.findAll();
    }

    public Mono<Product> getById(int id){
        return productRepository.findById(id).switchIfEmpty(Mono.error(new CustomException(HttpStatus.NOT_FOUND,NF_MESSAGE)));
    }

    public Mono<Product> save(ProductDto dto){
        Mono<Boolean> existName = productRepository.findByName(dto.getName()).hasElement();
        return existName.flatMap(exists -> exists ? Mono.error(new CustomException(HttpStatus.BAD_REQUEST,NAME_MESSAGE))
                : productRepository.save(Product.builder().name(dto.getName()).price(dto.getPrice()).build()));
    }

    public Mono<Product> update(int id, ProductDto dto){
        Mono<Boolean> productId = productRepository.findById(id).hasElement();
        Mono<Boolean> productRepeatedName = productRepository.repeatedName(id,dto.getName()).hasElement();
        return productId.flatMap(
            existsId -> existsId ?
                    productRepeatedName.flatMap(
                            existsName -> existsName ?
                                    Mono.error(new CustomException(HttpStatus.BAD_REQUEST,NAME_MESSAGE))
                            : productRepository.save(Product.builder().name(dto.getName()).price(dto.getPrice()).build()))
        : Mono.error(new CustomException(HttpStatus.NOT_FOUND,NF_MESSAGE)));
    }

    public Mono<Void> delete(int id){
        Mono<Boolean> productId = productRepository.findById(id).hasElement();
        return productId.flatMap(exists -> exists ? productRepository.deleteById(id) :
                Mono.error(new CustomException(HttpStatus.NOT_FOUND,NF_MESSAGE)));
    }
}
