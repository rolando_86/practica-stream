package com.example.demo;

import com.example.demo.models.Employee;
import com.example.demo.repositories.RepositorioEmpleados;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.fasterxml.jackson.databind.type.LogicalType.Collection;

@SpringBootApplication
public class DemoApplication {

	private static Employee[] arrayOfEmps = {
			new Employee(1, "Jeff Bezos", 1000000.0),
			new Employee(2, "Bill Gates", 2000000.0),
			new Employee(3, "Mark Zuckerberg", 3000000.0)
	};

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	public void metodosConStream(){
		Stream.of(arrayOfEmps);
		List<Employee> employeeList = Arrays.asList(arrayOfEmps);
		employeeList.stream();


		Stream.Builder<Employee> employeeBuilder = Stream.builder();

		employeeBuilder.accept(arrayOfEmps[0]);
		employeeBuilder.accept(arrayOfEmps[1]);
		employeeBuilder.accept(arrayOfEmps[2]);

		Stream<Employee> employeeStream = employeeBuilder.build();
		/**
		 * Stream.map()
		 *
		 * devuelve una secuencia que consta de los resultados de aplicar
		 * la función dada a los elementos de esta secuencia.
		 */
        /*List<String> employeeList1= employeeStream.map(e -> e.getName()).collect(Collectors.toList());
        System.out.println(employeeList1);

        List<Double> employeeList2= employeeStream.map(e -> e.getMoney()).collect(Collectors.toList());
        System.out.println(employeeList2);

        List<Integer> employeeList3= employeeStream.map(e -> e.getId()).collect(Collectors.toList());
        System.out.println(employeeList3);*/

		Integer[] integers = {1,2,3};
		RepositorioEmpleados repositorioEmpleados = new RepositorioEmpleados();
		List<Employee> emplList = Stream.of(integers).map( repositorioEmpleados::findById).collect(Collectors.toList());



		/**
		 * forEach()
		 *
		 * Operacion simple y comun; recorre los elementos de la secuencia
		 * y llama a la funcion proporcionada en cada elemento
		 *
		 */



		/*List<Employee> employeeList4 =  Arrays.stream(arrayOfEmps).collect(Collectors.toList());
		employeeList4.forEach(e -> e.sumarMoney(3000000.0));
		/**
		 * Por lo tanto, solo podemos realizar una única operación que consume un Stream ;
		 * de lo contrario, obtendremos una excepción que indica que el Stream ya ha sido operado o cerrado.
		En pocas palabras, la solución consiste en crear un nuevo Stream cada vez que lo necesitemos
		employeeList4.forEach(e-> System.out.println(e.getMoney()));*/

		List<Employee> employeeList4 =  Arrays.stream(arrayOfEmps).collect(Collectors.toList());
		employeeList4.forEach(e -> {
			e.sumarMoney(3000000.0);
			System.out.println(e.getMoney());
		});

		Supplier<Stream<String>> streamSupplier
				= () -> Stream.of("A", "B", "C", "D");
		Optional<String> result1 = streamSupplier.get().findAny();
		System.out.println(((Optional<?>) result1).get());
		Optional<String> result2 = streamSupplier.get().findFirst();
		System.out.println(result2.get());

		/**
		 * Collect
		 *
		 * para sacar cosas del flujo una vez que hayamos terminado con todo el procedimiento
		 *
		 *
		 */

		List<Employee> employee = employeeList.stream().collect(Collectors.toList());
		System.out.println("employee size:" + employee.size());

		/**
		 * filter
		 *
		 * produce una nueva secuencia que contiene elementos de la secuencia original que pasan
		 * una prueba determinada (especificada por un predicado)
		 */
		Stream.of(arrayOfEmps);
		List<Employee> employeeList7 = Arrays.asList(arrayOfEmps);
		employeeList7.stream().filter(e -> e.getMoney() > 5000000.0).collect(Collectors.toList()).forEach(
				e -> System.out.println(e.getMoney())
		);


		/**
		 * flatMap
		 *
		 * Una secuencia puede contener estructuras de datos complejas como Stream<List<String>> .
		 * En casos como este, flatMap() nos ayuda a aplanar la estructura de datos para
		 * simplificar operaciones adicionales.
		 *
		 *
		 */

		List<List<String>> nombresList = Arrays.asList(
				Arrays.asList("Jeff", "Bezos","rolando", "ruben"),
				Arrays.asList("Bill", "Gates","carlos"),
				Arrays.asList ("Mark Zuckerberg"));

		List<String> namesFlatStream = nombresList.stream()
				.flatMap(list -> list.stream())
				.collect(Collectors.toList());

		System.out.println("lista nombres: " + namesFlatStream);
		/**
		 *  convertir Stream<List<String>> en un Stream<String> más simple , utilizando la API flatMap() .
		 */
	}



}
