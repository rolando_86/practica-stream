package com.example.demo.handler;

import com.example.demo.dto.ProductDto;
import com.example.demo.entity.Product;
import com.example.demo.service.ProductService;
import com.example.demo.validation.ObjectValidator;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class ProductHandler {

    private ProductService productService;
    private ObjectValidator objectValidator;

    public ProductHandler(ProductService productService, ObjectValidator objectValidator){
        this.productService = productService;
        this.objectValidator = objectValidator;
    }

    public Mono<ServerResponse> getAll (ServerRequest request){
        Flux<Product> products = productService.getAll();
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(products,Product.class);
    }

    public Mono<ServerResponse> getOne(ServerRequest request){
        int id = Integer.valueOf(request.pathVariable("id"));
        Mono<Product> product = productService.getById(id);
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(product,Product.class);
    }

    public Mono<ServerResponse> save(ServerRequest request){
        Mono<ProductDto> productDtoMono = request.bodyToMono(ProductDto.class).doOnNext(objectValidator::validate);
        return productDtoMono.flatMap(productDto -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(productService.save(productDto),Product.class));
    }

    public Mono<ServerResponse> update(ServerRequest request){
        int id = Integer.valueOf(request.pathVariable("id"));
        Mono<ProductDto> productDtoMono = request.bodyToMono(ProductDto.class).doOnNext(objectValidator::validate);
        return productDtoMono.flatMap(productDto -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(productService.update(id,productDto),Product.class));
    }

    public Mono<ServerResponse> delete(ServerRequest request){
        int id = Integer.valueOf(request.pathVariable("id"));
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(productService.delete(id),Product.class);
    }

}
